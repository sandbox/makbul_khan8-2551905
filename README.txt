README.txt
==========

Commerce Cashback
A module providing cashback facility based on user's online transactions. 

INSTRUCTIONS
======================
1. Download and enable the module from module via /admin/modules or
through drush command: drush en commerce_cashback.
2. Click on configure link from /admin/modules or from /admin/config to
configure module details.


AUTHOR/MAINTAINER
======================
-Makbul Khan 
======================
